test-all:
	mvn clean verify
run-app:
	mvn spring-boot:run -Dspring-boot.run.arguments="--spring.profiles.active=local"