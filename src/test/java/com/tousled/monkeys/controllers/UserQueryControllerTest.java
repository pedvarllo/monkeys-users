package com.tousled.monkeys.controllers;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserResponse;
import com.tousled.monkeys.services.UserQueryService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class UserQueryControllerTest {

    private UserQueryService userQueryService;
    private ConversionService conversionService;
    private UserQueryController cut;

    @BeforeEach
    void setUp() {
        this.userQueryService = mock(UserQueryService.class);
        this.conversionService = mock(ConversionService.class);
        this.cut = new UserQueryController(userQueryService, conversionService);
    }

    @Test
    void getAllUsers() {
        UserResponse userResponse = UserResponse.builder().build();
        User user = User.builder().build();

        when(userQueryService.getAllUsers()).thenReturn(List.of(user));
        when(conversionService.convert(any(User.class), eq(UserResponse.class))).thenReturn(userResponse);

        ResponseEntity<List<UserResponse>> actualResponse = this.cut.getAllUsers();
        assertEquals(HttpStatus.OK, actualResponse.getStatusCode());
        assertEquals(1, actualResponse.getBody().size());
    }

}