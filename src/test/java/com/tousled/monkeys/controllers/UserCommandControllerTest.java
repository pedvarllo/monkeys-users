package com.tousled.monkeys.controllers;

import com.tousled.monkeys.UserCommandRequestObjectMotherBuilder;
import com.tousled.monkeys.UserUpdateCommandRequestObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.*;
import com.tousled.monkeys.services.UserRegistrationService;
import com.tousled.monkeys.services.UserUpdateService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;

class UserCommandControllerTest {

    private UserRegistrationService userRegistrationService;
    private UserUpdateService userUpdateService;
    private ConversionService conversionService;
    private UserCommandController cut;

    @BeforeEach
    void setUp() {
        this.userRegistrationService = mock(UserRegistrationService.class);
        this.userUpdateService = mock(UserUpdateService.class);
        this.conversionService = mock(ConversionService.class);
        this.cut = new UserCommandController(userRegistrationService, userUpdateService, conversionService);
    }

    @Test
    void itShouldRegisterUser() {

        UserCommandRequest userCommandRequest = UserCommandRequestObjectMotherBuilder
                .createCommand("Elton", "Smith", "email@monkeys.com", UserRole.ROLE_ADMIN);
        User userConverted = User.builder().build();
        UUID userCreatedId = UUID.randomUUID();
        UserCommandResponse expectedResponse = UserCommandResponse.builder().userId(userCreatedId).build();

        when(conversionService.convert(userCommandRequest, User.class)).thenReturn(userConverted);
        when(userRegistrationService.registerUser(userConverted)).thenReturn(userCreatedId);

        ResponseEntity<UserCommandResponse> actualResponse = this.cut.registerUser(userCommandRequest);

        assertEquals(HttpStatus.CREATED, actualResponse.getStatusCode());
        assertNotNull(actualResponse);
        assertEquals(expectedResponse, actualResponse.getBody());

    }

    @Test
    void itShouldUpdateUser() {

        UserUpdateCommandRequest userUpdateCommandRequest = UserUpdateCommandRequestObjectMotherBuilder
                .createUpdateRequest("Elton", "Smith", "email@monkeys.com");

        when(conversionService.convert(userUpdateCommandRequest, User.class)).thenReturn(User.builder().build());
        doNothing().when(userUpdateService).updateUser(any(User.class));

        ResponseEntity<Void> actualResponse = this.cut.updateUser(userUpdateCommandRequest);
        assertEquals(HttpStatus.NO_CONTENT, actualResponse.getStatusCode());
    }

    @Test
    void itShouldReturnDeleteOkNoContent() {

        UUID userToDelete = UUID.randomUUID();
        ResponseEntity<Void> actualResponse = this.cut.deleteUser(userToDelete);

        verify(userRegistrationService).unregisterUser(userToDelete);
        assertEquals(HttpStatus.NO_CONTENT, actualResponse.getStatusCode());

    }

    @Test
    void itShouldReturnRoleChangedOkNoContent() {

        UUID userId = UUID.randomUUID();
        RoleChangeRequest userRole = RoleChangeRequest.builder().userRole("ROLE_ADMIN").build();

        ResponseEntity<Void> actualResponse = this.cut.changeRole(userRole, userId);

        verify(userUpdateService).changeRole(userId, userRole.getUserRole());

        assertEquals(HttpStatus.NO_CONTENT, actualResponse.getStatusCode());

    }
}