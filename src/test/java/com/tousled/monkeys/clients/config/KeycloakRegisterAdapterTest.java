package com.tousled.monkeys.clients.config;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.theagilemonkeys.exceptions.IntegrationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.*;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;

import javax.ws.rs.core.Response;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

class KeycloakRegisterAdapterTest {

    private static final String REGISTRATION_ERROR_MESSAGE = "The user could not be created in Keycloak. Response: ";
    private static final String UNSUBSCRIBE_ERROR_MESSAGE = "The user could not be unsubscribed in Keycloak. Response: ";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    private String realm;
    private String clientId;
    private KeycloakProvider keycloakProvider;
    private ConversionService conversionService;
    private UsersResource usersResource;
    private KeycloakRegisterAdapter cut;

    @BeforeEach
    void setUp() {
        this.realm = "realm";
        this.clientId = "clientId";
        this.keycloakProvider = mock(KeycloakProvider.class);
        this.conversionService = mock(ConversionService.class);
        this.cut = new KeycloakRegisterAdapter(realm, clientId, keycloakProvider, conversionService);

        Keycloak keycloak = mock(Keycloak.class);
        RealmResource realmResource = mock(RealmResource.class);

        this.usersResource = mock(UsersResource.class);
        when(keycloakProvider.getInstance()).thenReturn(keycloak);
        when(keycloak.realm(eq(realm))).thenReturn(realmResource);
        when(realmResource.users()).thenReturn(usersResource);
    }

    @Test
    void itShouldNotRegisterIfError() {

        Keycloak keycloak = mock(Keycloak.class);
        RealmResource realmResource = mock(RealmResource.class);
        UsersResource usersResource = mock(UsersResource.class);
        UserRepresentation userRepresentation = new UserRepresentation();
        User user = mock(User.class);
        int httpStatus = HttpStatus.FORBIDDEN.value();

        when(keycloakProvider.getInstance()).thenReturn(keycloak);
        when(keycloak.realm(eq(realm))).thenReturn(realmResource);
        when(realmResource.users()).thenReturn(usersResource);

        when(conversionService.convert(user, UserRepresentation.class)).thenReturn(userRepresentation);
        when(usersResource.create(userRepresentation)).thenReturn(Response.status(httpStatus).build());

        Exception exception = assertThrows(Exception.class, () -> this.cut.registerUser(user, ROLE_ADMIN));

        assertEquals(IntegrationException.class, exception.getClass());
        assertEquals(REGISTRATION_ERROR_MESSAGE + httpStatus, exception.getMessage());

    }

    @Test
    void itShouldNotUnregisterIfError() {

        User user = mock(User.class);
        int httpStatus = HttpStatus.FORBIDDEN.value();

        when(usersResource.delete(user.getSecurityProviderId())).thenReturn(Response.status(httpStatus).build());

        Exception exception = assertThrows(Exception.class, () -> this.cut.unregisterUser(user));

        assertEquals(IntegrationException.class, exception.getClass());
        assertEquals(UNSUBSCRIBE_ERROR_MESSAGE + httpStatus, exception.getMessage());

    }

    @Test
    void itShouldUpdateUser() {

        User user = mock(User.class);
        UserRepresentation userRepresentation = mock(UserRepresentation.class);
        UserResource users = mock(UserResource.class);

        when(conversionService.convert(user, UserRepresentation.class)).thenReturn(userRepresentation);
        when(usersResource.get(user.getSecurityProviderId())).thenReturn(users);
        this.cut.updateUser(user);

        verify(usersResource).get(user.getSecurityProviderId());
        verify(users).update(userRepresentation);

    }

}