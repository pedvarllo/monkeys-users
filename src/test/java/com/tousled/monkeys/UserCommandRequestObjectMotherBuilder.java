package com.tousled.monkeys;

import com.tousled.monkeys.domain.dto.UserCommandRequest;
import com.tousled.monkeys.domain.dto.UserRole;

public class UserCommandRequestObjectMotherBuilder {

    public static UserCommandRequest createCommand(String name, String surname, String email, UserRole role) {
        return  UserCommandRequest.builder()
                .name(name)
                .surname(surname)
                .email(email)
                .userRole(role)
                .build();
    }
}
