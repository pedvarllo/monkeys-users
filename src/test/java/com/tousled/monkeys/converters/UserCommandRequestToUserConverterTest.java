package com.tousled.monkeys.converters;

import com.tousled.monkeys.UserCommandRequestObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserCommandRequest;
import com.tousled.monkeys.domain.dto.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UserCommandRequestToUserConverterTest {

    private static final String CONVERSION_ERROR_MSG = "User role is not known. You can use: 'ROLE_ADMIN' or 'ROLE_USER'";

    private UserCommandRequestToUserConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new UserCommandRequestToUserConverter();
    }

    @Test
    void itShouldConvertFromUserCommandControllerToUser() {
        UserCommandRequest command = UserCommandRequestObjectMotherBuilder
                .createCommand("John", "Taylor", "john.taylor@monkeys.com", UserRole.ROLE_ADMIN);

        User actualResponse = this.cut.convert(command);

        assertEquals(command.getName(), actualResponse.getName());
        assertEquals(command.getSurname(), actualResponse.getSurname());
        assertEquals(command.getEmail(), actualResponse.getEmail());
        assertEquals(command.getPassword(), actualResponse.getPassword());
        assertEquals(command.getUserRole().name(), actualResponse.getRole().name());
    }

    @Test
    void itShouldNotConvertFromUserCommandControllerToUserIfRoleIsNotKnown() {
        UserCommandRequest command = UserCommandRequestObjectMotherBuilder
                .createCommand("John", "Taylor", "john.taylor@monkeys.com", UserRole.ROLE_UNKNOWN);

        Exception exception = assertThrows(Exception.class, () -> this.cut.convert(command));

        assertEquals(exception.getMessage(), CONVERSION_ERROR_MSG);
        assertEquals(exception.getClass(), IllegalArgumentException.class);

    }
}