package com.tousled.monkeys.converters;

import com.tousled.monkeys.UserObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import static org.junit.jupiter.api.Assertions.*;

class UserToUserRepresentationConverterTest {

    private UserToUserRepresentationConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new UserToUserRepresentationConverter();
    }

    @Test
    void itShouldConvertFromUserToUserRepresentation() {
        User user = UserObjectMotherBuilder.createUser("Frank", "Parrot", "frank_parrot@monkeys.com", UserRole.ROLE_USER);

        UserRepresentation actualResponse = this.cut.convert(user);

        assertNotNull(actualResponse);
        assertEquals(user.getName(), actualResponse.getFirstName());
        assertEquals(user.getSurname(), actualResponse.getLastName());
        assertEquals(user.getEmail(), actualResponse.getEmail());
        assertEquals("frank_parrot", actualResponse.getUsername());
        assertEquals(createPasswordCredentials(user.getPassword()), actualResponse.getCredentials().get(0));
        assertTrue(actualResponse.isEnabled());
    }

    private static CredentialRepresentation createPasswordCredentials(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }
}