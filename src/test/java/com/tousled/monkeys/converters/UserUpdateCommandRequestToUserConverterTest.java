package com.tousled.monkeys.converters;

import com.tousled.monkeys.UserUpdateCommandRequestObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserUpdateCommandRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class UserUpdateCommandRequestToUserConverterTest {

    private UserUpdateCommandRequestToUserConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new UserUpdateCommandRequestToUserConverter();
    }

    @Test
    void itShouldConvertFromUserUpdateCommandRequestToUser() {

        UserUpdateCommandRequest userUpdateCommandRequest = UserUpdateCommandRequestObjectMotherBuilder
                .createUpdateRequest("Dua", "Lipa", "dua.lipa@monkeys.com");

        User actualResponse = this.cut.convert(userUpdateCommandRequest);

        assertEquals(userUpdateCommandRequest.getId(), actualResponse.getId());
        assertEquals(userUpdateCommandRequest.getName(), actualResponse.getName());
        assertEquals(userUpdateCommandRequest.getSurname(), actualResponse.getSurname());
        assertEquals(userUpdateCommandRequest.getEmail(), actualResponse.getEmail());
        assertNull(actualResponse.getRole());
    }
}