package com.tousled.monkeys.converters;

import com.tousled.monkeys.UserObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.domain.dto.UserResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UserToUserResponseConverterTest {

    private UserToUserResponseConverter cut;

    @BeforeEach
    void setUp() {
        this.cut = new UserToUserResponseConverter();
    }

    @Test
    void itShouldConvertFromUserToUserResponse() {
        User user = UserObjectMotherBuilder.createUser("name", "surname", "email", UserRole.ROLE_USER);

        UserResponse actualResponse = this.cut.convert(user);

        assertEquals(user.getId(), actualResponse.getId());
        assertEquals(user.getName(), actualResponse.getName());
        assertEquals(user.getSurname(), actualResponse.getSurname());
        assertEquals(user.getEmail(), actualResponse.getEmail());
        assertEquals(user.getRole().name(), actualResponse.getRole().name());

    }
}