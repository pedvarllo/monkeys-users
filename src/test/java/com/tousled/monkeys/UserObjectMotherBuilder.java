package com.tousled.monkeys;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;

import java.util.UUID;

public class UserObjectMotherBuilder {

    public static User createUser(String name, String surname, String email, UserRole role) {
        return User.builder()
                .id(UUID.randomUUID())
                .name(name)
                .surname(surname)
                .email(email)
                .role(role)
                .build();
    }
}
