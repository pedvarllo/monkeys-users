package com.tousled.monkeys;

import com.tousled.monkeys.domain.dto.UserUpdateCommandRequest;

import java.util.UUID;

public class UserUpdateCommandRequestObjectMotherBuilder {

    public static UserUpdateCommandRequest createUpdateRequest(String name, String surname, String email) {
        return UserUpdateCommandRequest.builder()
                .id(UUID.randomUUID())
                .name(name)
                .surname(surname)
                .email(email)
                .build();
    }
}
