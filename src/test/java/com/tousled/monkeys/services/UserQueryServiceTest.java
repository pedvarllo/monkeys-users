package com.tousled.monkeys.services;

import com.tousled.monkeys.UserObjectMotherBuilder;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.repositories.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class UserQueryServiceTest {

    private UserRepository userRepository;
    private UserQueryService sut;

    @BeforeEach
    void setUp() {
        this.userRepository = mock(UserRepository.class);
        this.sut = new UserQueryService(this.userRepository);
    }

    @Test
    void itShouldRetrieveAllUsers() {

        User user = UserObjectMotherBuilder.createUser("username", "surname", "email@monkeys.com", UserRole.ROLE_USER);
        when(userRepository.findAll()).thenReturn(List.of(user));

        List<User> actualResponse = this.sut.getAllUsers();

        verify(userRepository).findAll();
        assertEquals(1, actualResponse.size());
        assertEquals(user, actualResponse.get(0));
    }
}