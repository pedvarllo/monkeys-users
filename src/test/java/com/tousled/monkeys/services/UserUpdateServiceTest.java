package com.tousled.monkeys.services;

import com.tousled.monkeys.UserObjectMotherBuilder;
import com.tousled.monkeys.clients.config.SecurityProvider;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.repositories.UserRepository;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class UserUpdateServiceTest {

    private static final String USER_NOT_FOUND_ERROR_MESSAGE = "User with id %s not found";
    private static final String ROLE_NOT_FOUND_ERROR_MESSAGE = "Role does not exist. Please try another one";

    private SecurityProvider securityProvider;
    private UserRepository userRepository;
    private UserUpdateService sut;

    @BeforeEach
    void setUp() {
        this.securityProvider = mock(SecurityProvider.class);
        this.userRepository = mock(UserRepository.class);
        this.sut = new UserUpdateService(securityProvider, userRepository);
    }

    @Test
    void itShouldUpdateUser() {
        User user = UserObjectMotherBuilder.createUser("new name", "surname", "new name.surname@monkeys.com", UserRole.ROLE_USER);
        User oldUser = UserObjectMotherBuilder.createUser("name", "surname", "name.surname@monkeys.com", UserRole.ROLE_USER);

        when(userRepository.findById(user.getId())).thenReturn(Optional.of(oldUser));
        doNothing().when(securityProvider).updateUser(user);

        this.sut.updateUser(user);

        User userToSave = updateUserInfo(user, oldUser);
        verify(securityProvider).updateUser(userToSave);
        verify(userRepository).save(userToSave);
    }

    @Test
    void itShouldNotUpdateUserIfTheyAreNotFound() {
        User user = mock(User.class);

        when(userRepository.findById(user.getId())).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.updateUser(user));

        verifyNoInteractions(securityProvider);
        verify(userRepository).findById(user.getId());

        assertEquals(NotFoundException.class, exception.getClass());
        assertEquals(String.format(USER_NOT_FOUND_ERROR_MESSAGE, user.getId()), exception.getMessage());
    }

    @Test
    void itShouldThrowAnExceptionIfTheRoleDoesNotExist() {

        String userRole = "NOT_ROLE";
        UUID userId = UUID.randomUUID();

        Exception exception = assertThrows(Exception.class, () -> this.sut.changeRole(userId, userRole));

        verifyNoInteractions(userRepository);
        verifyNoInteractions(securityProvider);

        assertEquals(NotFoundException.class, exception.getClass());
        assertEquals(ROLE_NOT_FOUND_ERROR_MESSAGE, exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource(value = "getValidUserRoles")
    void itShouldThrowAnExceptionIfTheUserDoesNotExist(String userRole) {

        UUID userId = UUID.randomUUID();

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.changeRole(userId, userRole));

        verify(userRepository).findById(userId);
        verifyNoInteractions(securityProvider);

        assertEquals(NotFoundException.class, exception.getClass());
        assertEquals(String.format(USER_NOT_FOUND_ERROR_MESSAGE, userId), exception.getMessage());
    }

    @ParameterizedTest
    @MethodSource(value = "getValidUserRoles")
    void itShouldChangeTheUserRoleIfRoleIsValidAndUserExist(String userRole) {

        User oldUser = UserObjectMotherBuilder.createUser("name", "surname", "email", UserRole.ROLE_USER);

        when(userRepository.findById(oldUser.getId())).thenReturn(Optional.of(oldUser));

        this.sut.changeRole(oldUser.getId(), userRole);

        oldUser.setRole(UserRole.valueOf(userRole));

        verify(userRepository).findById(oldUser.getId());
        verify(securityProvider).changeRole(oldUser, oldUser.getRole().name());
        verify(userRepository).save(oldUser);
    }

    private User updateUserInfo(User newUser, User oldUser) {
        return User.builder()
                .id(oldUser.getId())
                .securityProviderId(oldUser.getSecurityProviderId())
                .name(newUser.getName())
                .surname(newUser.getSurname())
                .email(newUser.getEmail())
                .role(oldUser.getRole())
                .build();
    }

    private static List<String> getValidUserRoles() {
        return List.of("ROLE_ADMIN", "ROLE_USER");
    }

}