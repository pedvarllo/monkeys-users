package com.tousled.monkeys.services;

import com.tousled.monkeys.UserObjectMotherBuilder;
import com.tousled.monkeys.clients.config.KeycloakRegisterAdapter;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.repositories.UserRepository;
import com.tousled.theagilemonkeys.exceptions.IntegrationException;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class UserRegistrationServiceTest {

    private static final String USER_NOT_FOUND_ERROR_MESSAGE = "User with id %s not found";

    private UserRepository userRepository;
    private KeycloakRegisterAdapter keycloakRegisterAdapter;
    private UserRegistrationService sut;

    @BeforeEach
    void setUp() {
        this.userRepository = mock(UserRepository.class);
        this.keycloakRegisterAdapter = mock(KeycloakRegisterAdapter.class);
        this.sut = new UserRegistrationService(userRepository, keycloakRegisterAdapter);
    }

    @Test
    void itShouldRegistryUser() {

        User user = UserObjectMotherBuilder.createUser("Boris", "Smith", "boris_smith@monkeys.com", UserRole.ROLE_USER);
        User savedUser = UserObjectMotherBuilder.createUser("Boris", "Smith", "boris_smith@monkeys.com", UserRole.ROLE_USER);
        UUID savedUserId = UUID.randomUUID();
        String userProviderId = UUID.randomUUID().toString();

        savedUser.setId(savedUserId);
        when(keycloakRegisterAdapter.registerUser(user, UserRole.ROLE_USER.name())).thenReturn(userProviderId);
        when(userRepository.save(user)).thenReturn(savedUser);

        UUID actualResponse = this.sut.registerUser(user);

        user.setSecurityProviderId(userProviderId);
        verify(userRepository).save(user);
        assertEquals(savedUserId, actualResponse);
    }

    @Test
    void itShouldNotSaveUserInDBIfProviderFails() {

        User user = UserObjectMotherBuilder.createUser("Boris", "Smith", "boris_smith@monkeys.com", UserRole.ROLE_USER);

        when(keycloakRegisterAdapter.registerUser(user, UserRole.ROLE_USER.name())).thenThrow(new IntegrationException());

        Exception exception = assertThrows(Exception.class, () -> this.sut.registerUser(user));

        assertEquals(IntegrationException.class, exception.getClass());
        verifyNoInteractions(userRepository);
    }

    @Test
    void itShouldUnregisterUser() {
        UUID userId = UUID.randomUUID();
        User user = mock(User.class);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));
        doNothing().when(keycloakRegisterAdapter).unregisterUser(user);

        this.sut.unregisterUser(userId);

        verify(keycloakRegisterAdapter).unregisterUser(user);
        verify(userRepository).findById(userId);
        verify(userRepository).delete(user);
    }

    @Test
    void itShouldNotUnregisterUserIfUserIsNotFound() {
        UUID userId = UUID.randomUUID();

        when(userRepository.findById(userId)).thenReturn(Optional.empty());

        Exception exception = assertThrows(Exception.class, () -> this.sut.unregisterUser(userId));

        verify(userRepository).findById(userId);
        verifyNoInteractions(keycloakRegisterAdapter);
        assertEquals(NotFoundException.class, exception.getClass());
        assertEquals(String.format(USER_NOT_FOUND_ERROR_MESSAGE, userId), exception.getMessage());
    }

    @Test
    void itShouldNotUnregisterUserIfProviderFails() {
        UUID userId = UUID.randomUUID();
        User user = mock(User.class);

        when(userRepository.findById(userId)).thenReturn(Optional.of(user));

        doThrow(IntegrationException.class).when(keycloakRegisterAdapter).unregisterUser(user);

        Exception exception = assertThrows(Exception.class, () -> this.sut.unregisterUser(userId));

        verify(keycloakRegisterAdapter).unregisterUser(user);
        verify(userRepository).findById(userId);
        verify(userRepository, never()).delete(any());

        assertEquals(IntegrationException.class, exception.getClass());
    }
}