package com.tousled.monkeys.converters;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.domain.dto.UserCommandRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserCommandRequestToUserConverter implements Converter<UserCommandRequest, User> {

    private static final String CONVERSION_ERROR_MSG = "User role is not known. You can use: 'ROLE_ADMIN' or 'ROLE_USER'";

    @Override
    public User convert(UserCommandRequest source) {

        if (UserRole.contains(source.getUserRole().name())) {
            return User.builder()
                    .name(source.getName())
                    .surname(source.getSurname())
                    .email(source.getEmail())
                    .password(source.getPassword())
                    .role(UserRole.valueOf(source.getUserRole().name()))
                    .build();
        }

        throw new IllegalArgumentException(CONVERSION_ERROR_MSG);

    }
}
