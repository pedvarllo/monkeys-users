package com.tousled.monkeys.converters;

import com.tousled.monkeys.domain.User;
import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class UserToUserRepresentationConverter implements Converter<User, UserRepresentation> {

    @Override
    public UserRepresentation convert(User source) {
        String username = source.getName() + "_" + source.getSurname();
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setFirstName(source.getName());
        userRepresentation.setLastName(source.getSurname());
        userRepresentation.setEmail(source.getEmail());
        userRepresentation.setUsername(username.toLowerCase());
        userRepresentation.setCredentials(List.of(createPassword(source.getPassword())));
        userRepresentation.setEnabled(true);
        return userRepresentation;
    }

    private CredentialRepresentation createPassword(String password) {
        CredentialRepresentation passwordCredentials = new CredentialRepresentation();
        passwordCredentials.setTemporary(false);
        passwordCredentials.setType(CredentialRepresentation.PASSWORD);
        passwordCredentials.setValue(password);
        return passwordCredentials;
    }
}
