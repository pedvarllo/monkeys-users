package com.tousled.monkeys.converters;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserUpdateCommandRequest;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserUpdateCommandRequestToUserConverter implements Converter<UserUpdateCommandRequest, User> {

    @Override
    public User convert(UserUpdateCommandRequest source) {
        return User.builder()
                .id(source.getId())
                .name(source.getName())
                .surname(source.getSurname())
                .email(source.getEmail())
                .build();
    }
}
