package com.tousled.monkeys.converters;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserResponse;
import com.tousled.monkeys.domain.dto.UserRole;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserResponseConverter implements Converter<User, UserResponse> {

    @Override
    public UserResponse convert(User source) {
        return UserResponse.builder()
                .id(source.getId())
                .name(source.getName())
                .surname(source.getSurname())
                .email(source.getEmail())
                .role(UserRole.valueOf(source.getRole().name()))
                .build();
    }
}
