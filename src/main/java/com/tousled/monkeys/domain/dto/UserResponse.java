package com.tousled.monkeys.domain.dto;

import lombok.Builder;
import lombok.Data;

import java.util.Objects;
import java.util.UUID;

@Data
@Builder
public class UserResponse {

    private UUID id;

    private String name;

    private String surname;

    private String email;

    private UserRole role;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return id.equals(that.id) && name.equals(that.name) && surname.equals(that.surname) && email.equals(that.email) && role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, surname, email, role);
    }
}
