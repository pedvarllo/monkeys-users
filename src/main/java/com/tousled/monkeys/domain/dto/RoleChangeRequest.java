package com.tousled.monkeys.domain.dto;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

@Data
@Builder
public class RoleChangeRequest {

    private String userRole;

    @Tolerate
    public RoleChangeRequest() {}
}
