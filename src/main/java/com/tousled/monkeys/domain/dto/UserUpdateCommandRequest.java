package com.tousled.monkeys.domain.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.UUID;

@Data
@Builder
public class UserUpdateCommandRequest {

    @NotNull
    private UUID id;
    @NotNull
    private String name;
    @NotNull
    private String surname;
    @Email
    @NotNull
    private String email;

}
