package com.tousled.monkeys.domain.dto;

public enum UserRole {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN"),
    ROLE_UNKNOWN("UNKNOWN");

    private final String label;

    UserRole(String label) {
        this.label = label;
    }

}
