package com.tousled.monkeys.domain.dto;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class UserCommandRequest {

    @NotNull
    private String name;
    @NotNull
    private String surname;
    @NotNull
    private String password;
    @Email
    @NotNull
    private String email;
    @NotNull
    private UserRole userRole;
}
