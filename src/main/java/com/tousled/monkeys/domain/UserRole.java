package com.tousled.monkeys.domain;

import java.util.Arrays;

public enum UserRole {

    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN");

    private final String label;

    UserRole(String label) {
        this.label = label;
    }

    public static boolean contains(String test) {
        return Arrays.stream(UserRole.values()).anyMatch(c -> c.name().equals(test));
    }
}
