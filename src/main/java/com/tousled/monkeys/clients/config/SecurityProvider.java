package com.tousled.monkeys.clients.config;

import com.tousled.monkeys.domain.User;

public interface SecurityProvider {

    String registerUser(User user, String role);
    void unregisterUser(User user);
    void updateUser(User user);
    void changeRole(User user, String userRole);
}
