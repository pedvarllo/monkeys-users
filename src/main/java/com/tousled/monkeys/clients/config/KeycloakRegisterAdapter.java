package com.tousled.monkeys.clients.config;

import com.tousled.monkeys.domain.User;
import com.tousled.theagilemonkeys.exceptions.IntegrationException;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.UserResource;
import org.keycloak.admin.client.resource.UsersResource;
import org.keycloak.representations.idm.ClientRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.util.List;

@Component
public class KeycloakRegisterAdapter implements SecurityProvider {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakRegisterAdapter.class);
    private static final String REGISTRATION_ERROR_MESSAGE = "The user could not be created in Keycloak. Response: ";
    private static final String UNSUBSCRIBE_ERROR_MESSAGE = "The user could not be unsubscribed in Keycloak. Response: ";
    private static final String LOCATION_HEADER = "Location";

    private final String realm;
    private final String clientId;
    private final KeycloakProvider keycloakProvider;
    private final ConversionService conversionService;


    @Autowired
    public KeycloakRegisterAdapter(@Value("${keycloak.realm}") String realm,
                                   @Value("${keycloak.resource}") String clientId,
                                   KeycloakProvider keycloakProvider,
                                   ConversionService conversionService) {
        this.realm = realm;
        this.clientId = clientId;
        this.keycloakProvider = keycloakProvider;
        this.conversionService = conversionService;
    }

    @Override
    public String registerUser(User user, String role) {
        UserRepresentation keycloakUser = this.conversionService.convert(user, UserRepresentation.class);
        UsersResource instance = getInstance().realm(realm).users();
        Response response = instance.create(keycloakUser);
        if (response.getStatus() != HttpStatus.CREATED.value()) {
            LOGGER.error("User could not be registered in our IAM Provider");
            throw new IntegrationException(REGISTRATION_ERROR_MESSAGE + response.getStatus());
        }
        LOGGER.info("User registered in our IAM Provider");
        String keycloakUserId = retrieveKeycloakUserId(response);
        RoleRepresentation roleRepresentation = findRoleByName(role, keycloakUserId);
        LOGGER.info("Assigning roles to our user");
        addRoleToUser(roleRepresentation, keycloakUserId);
        return keycloakUserId;
    }

    @Override
    public void unregisterUser(User user) {
        UsersResource instance = getInstance().realm(realm).users();
        LOGGER.info("Unsubscribing user from our IAM Provider");
        Response response = instance.delete(user.getSecurityProviderId());
        if (response.getStatus() != HttpStatus.NO_CONTENT.value()) {
            LOGGER.error("The user could not be unsubscribed from our IAM Provider");
            throw new IntegrationException(UNSUBSCRIBE_ERROR_MESSAGE + response.getStatus());
        }
    }

    @Override
    public void updateUser(User user) {
        UsersResource users = getInstance().realm(realm).users();
        UserRepresentation userRepresentation = this.conversionService.convert(user, UserRepresentation.class);
        UserResource userResource = users.get(user.getSecurityProviderId());
        LOGGER.info("Updating the user in our IAM Provider");
        userResource.update(userRepresentation);
    }

    @Override
    public void changeRole(User user, String userRole) {
        LOGGER.info("Updating the role of the user");
        String securityProviderId = user.getSecurityProviderId();
        RoleRepresentation oldRole = findRoleByName(user.getRole().name(), securityProviderId);
        RoleRepresentation newRole = findRoleByName(userRole, securityProviderId);

        LOGGER.info("Removing the old role...");
        removeRoleToUser(oldRole, securityProviderId);
        LOGGER.info("Adding the new role...");
        addRoleToUser(newRole, securityProviderId);
    }

    private String retrieveKeycloakUserId(Response response) {
        List<Object> locationHeaders = response.getHeaders().get(LOCATION_HEADER);
        if (locationHeaders != null && !locationHeaders.isEmpty()) {
            String location = locationHeaders.get(0).toString();
            return location.substring(location.lastIndexOf("/") + 1);
        }
        return null;
    }

    private void addRoleToUser(RoleRepresentation roleRepresentation, String keycloakUserId) {
        ClientRepresentation client = getClientRepresentation();

        getInstance().realm(realm).users().get(keycloakUserId)
                .roles()
                .clientLevel(client.getId())
                .add(List.of(roleRepresentation));
    }

    private void removeRoleToUser(RoleRepresentation roleRepresentation, String keycloakUserId) {
        ClientRepresentation client = getClientRepresentation();

        getInstance().realm(realm).users().get(keycloakUserId)
                .roles()
                .clientLevel(client.getId())
                .remove(List.of(roleRepresentation));
    }

    private RoleRepresentation findRoleByName(String userRole, String keycloakUserId) {
        ClientRepresentation client = getClientRepresentation();

        return getInstance().realm(realm)
                .clients()
                .get(client.getId())
                .roles().get(userRole).toRepresentation();
    }

    private Keycloak getInstance() {
        return this.keycloakProvider.getInstance();
    }

    private ClientRepresentation getClientRepresentation() {
        return getInstance().realm(realm).clients()
                .findByClientId(clientId).get(0);
    }
}
