package com.tousled.monkeys.controllers;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.RoleChangeRequest;
import com.tousled.monkeys.domain.dto.UserCommandRequest;
import com.tousled.monkeys.domain.dto.UserCommandResponse;
import com.tousled.monkeys.domain.dto.UserUpdateCommandRequest;
import com.tousled.monkeys.services.UserRegistrationService;
import com.tousled.monkeys.services.UserUpdateService;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1/users")
public class UserCommandController {

    private final UserRegistrationService userRegistrationService;
    private final UserUpdateService userUpdateService;
    private final ConversionService conversionService;

    @Autowired
    public UserCommandController(UserRegistrationService userRegistrationService,
                                 UserUpdateService userUpdateService,
                                 ConversionService conversionService) {
        this.userRegistrationService = userRegistrationService;
        this.userUpdateService = userUpdateService;
        this.conversionService = conversionService;
    }

    @PostMapping
    public ResponseEntity<UserCommandResponse> registerUser(@RequestBody UserCommandRequest userCommandRequest) {

        User user = this.conversionService.convert(userCommandRequest, User.class);
        UUID savedUserId = this.userRegistrationService.registerUser(user);
        UserCommandResponse bodyResponse = UserCommandResponse.builder().userId(savedUserId).build();
        return new ResponseEntity<>(bodyResponse, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Void> updateUser(@RequestBody UserUpdateCommandRequest userUpdateCommandRequest) {

        User user = this.conversionService.convert(userUpdateCommandRequest, User.class);
        this.userUpdateService.updateUser(user);
        return ResponseEntity.noContent().build();
    }

    @PatchMapping(path = "/{id}", consumes = "application/json")
    public ResponseEntity<Void> changeRole(@RequestBody RoleChangeRequest userRole, @PathVariable("id") UUID userId) {

        this.userUpdateService.changeRole(userId, userRole.getUserRole());
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable("id") UUID id) {

        this.userRegistrationService.unregisterUser(id);
        return ResponseEntity.noContent().build();
    }
}
