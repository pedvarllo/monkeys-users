package com.tousled.monkeys.controllers;

import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.dto.UserResponse;
import com.tousled.monkeys.services.UserQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/v1/users")
public class UserQueryController {

    private final UserQueryService userQueryService;
    private final ConversionService conversionService;

    @Autowired
    public UserQueryController(UserQueryService userQueryService, ConversionService conversionService) {
        this.userQueryService = userQueryService;
        this.conversionService = conversionService;
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> getAllUsers() {

        List<User> allUsers = this.userQueryService.getAllUsers();
        List<UserResponse> response = allUsers
                .stream()
                .map(u -> this.conversionService.convert(u, UserResponse.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok(response);
    }

}
