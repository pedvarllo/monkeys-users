package com.tousled.monkeys.services;

import com.tousled.monkeys.clients.config.SecurityProvider;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.domain.UserRole;
import com.tousled.monkeys.repositories.UserRepository;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserUpdateService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserUpdateService.class);

    private static final String USER_NOT_FOUND_ERROR_MESSAGE = "User with id %s not found";
    private static final String ROLE_NOT_FOUND_ERROR_MESSAGE = "Role does not exist. Please try another one";

    private final SecurityProvider securityProvider;
    private final UserRepository userRepository;

    @Autowired
    public UserUpdateService(SecurityProvider securityProvider, UserRepository userRepository) {
        this.securityProvider = securityProvider;
        this.userRepository = userRepository;
    }

    public void updateUser(User user) {

        Optional<User> optionalUser = this.userRepository.findById(user.getId());

        if (optionalUser.isEmpty()) {
            LOGGER.error("The user with id {} does not exist", user.getId());
            throw new NotFoundException(String.format(USER_NOT_FOUND_ERROR_MESSAGE, user.getId()));
        }

        User oldUser = optionalUser.get();
        User userToSave = updateUserInfo(user, oldUser);

        this.securityProvider.updateUser(userToSave);
        LOGGER.info("Updating user in our database");
        this.userRepository.save(userToSave);
    }

    public void changeRole(UUID userId, String userRole) {

        if (!UserRole.contains(userRole)) {
            LOGGER.error("The role {} does not exist", userRole);
            throw new NotFoundException(ROLE_NOT_FOUND_ERROR_MESSAGE);
        }

        UserRole newRole = UserRole.valueOf(userRole);

        Optional<User> optionalUser = userRepository.findById(userId);

        if (optionalUser.isEmpty()) {
            LOGGER.error("The user with id {} does not exist", userId);
            throw new NotFoundException(String.format(USER_NOT_FOUND_ERROR_MESSAGE, userId));
        }

        User user = optionalUser.get();
        this.securityProvider.changeRole(user, userRole);
        user.setRole(newRole);

        LOGGER.info("Changing role to the user in our database");
        this.userRepository.save(user);

    }

    private User updateUserInfo(User newUser, User oldUser) {
        return User.builder()
                .id(oldUser.getId())
                .securityProviderId(oldUser.getSecurityProviderId())
                .name(newUser.getName())
                .surname(newUser.getSurname())
                .email(newUser.getEmail())
                .password(oldUser.getPassword())
                .role(oldUser.getRole())
                .build();
    }
}
