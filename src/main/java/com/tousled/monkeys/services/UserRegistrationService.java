package com.tousled.monkeys.services;

import com.tousled.monkeys.clients.config.KeycloakRegisterAdapter;
import com.tousled.monkeys.clients.config.SecurityProvider;
import com.tousled.monkeys.domain.User;
import com.tousled.monkeys.repositories.UserRepository;
import com.tousled.theagilemonkeys.exceptions.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
public class UserRegistrationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRegistrationService.class);

    private final UserRepository userRepository;
    private final SecurityProvider securityProvider;

    @Autowired
    public UserRegistrationService(UserRepository userRepository,
                                   KeycloakRegisterAdapter securityProvider) {
        this.userRepository = userRepository;
        this.securityProvider = securityProvider;
    }

    public UUID registerUser(User user) {
        String keycloakUserId = this.securityProvider.registerUser(user, user.getRole().name());
        LOGGER.info("Saving user with security provider id {}", keycloakUserId);
        user.setSecurityProviderId(keycloakUserId);
        User savedUser = this.userRepository.save(user);
        return savedUser.getId();
    }

    public void unregisterUser(UUID id) {
        Optional<User> optionalUser = this.userRepository.findById(id);

        if (optionalUser.isEmpty()) {
            LOGGER.error("The user with id {} does not exist", id);
            throw new NotFoundException(String.format("User with id %s not found", id));
        }

        User userToDelete = optionalUser.get();
        this.securityProvider.unregisterUser(userToDelete);
        LOGGER.info("The customer with id {} has been deleted", id);
        this.userRepository.delete(userToDelete);
    }
}
